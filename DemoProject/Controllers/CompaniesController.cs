﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DemoProject.Data;
using DemoProject.Models;
using DemoProject.Services;

namespace DemoProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly DBContext _context;
        private readonly ICompanyServices _companyServices;

        public CompaniesController(DBContext context, ICompanyServices companyServices)
        {
            _context = context;
            _companyServices = companyServices;
        }

        // GET: api/Companies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Company>>> GetCompanies()
        {
            return await _context.Companies.ToListAsync();
        }

        // GET: api/Companies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Company>> GetCompany(int id)
        {
            var company = await _context.Companies.FindAsync(id);

            if (company == null)
            {
                return NotFound();
            }

            return company;
        }

        // GET: api/Companies/ABC/19082019/21082019/GPS
        /// <summary>
        /// Get selected company with all fouls in certain given time
        /// </summary>
        /// <param name="companyName">The company name</param>
        /// <param name="fromDate">The start date format ddMMyyyy</param>
        /// <param name="toDate">The end date format ddMMyyyy</param>
        /// <param name="foulType">Foul type is either Camera or GPS</param>
        /// <returns></returns>
        [HttpGet("{companyName}/{fromDate}/{toDate}/{foulType}")]
        public async Task<List<CompanyFoulDetails>> GetCompanyFoulDetails(string companyName, string fromDate, string toDate, string foulType)
        {
            var result = await _companyServices.GetCompanyFoulDetailsService(_context, companyName, fromDate, toDate, foulType);
            return result;
        }

        // PUT: api/Companies/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompany(int id, Company company)
        {
            if (id != company.ID)
            {
                return BadRequest();
            }

            _context.Entry(company).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Companies
        [HttpPost]
        public async Task<ActionResult<Company>> PostCompany(Company company)
        {
            _context.Companies.Add(company);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCompany", new { id = company.ID }, company);
        }

        // DELETE: api/Companies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Company>> DeleteCompany(int id)
        {
            var company = await _context.Companies.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            _context.Companies.Remove(company);
            await _context.SaveChangesAsync();

            return company;
        }

        private bool CompanyExists(int id)
        {
            return _context.Companies.Any(e => e.ID == id);
        }
    }
}
