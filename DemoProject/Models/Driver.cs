﻿using System.Collections.Generic;

namespace DemoProject.Models
{
    public class Driver
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int CompanyID { get; set; }

        public virtual Company Company { get; set; }
        public virtual ICollection<Foul> Foul { get; set; }
    }
}
