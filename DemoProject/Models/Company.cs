﻿using System.Collections.Generic;

namespace DemoProject.Models
{
    public class Company
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Driver> Drivers { get; set; }
    }
}
