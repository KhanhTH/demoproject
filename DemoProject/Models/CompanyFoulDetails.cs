﻿using System;

namespace DemoProject.Models
{
    public class CompanyFoulDetails
    {
        public string Name { get; set; }
        public DateTime FoulDate { get; set; }
        public string FoulType { get; set; }
    }
}
