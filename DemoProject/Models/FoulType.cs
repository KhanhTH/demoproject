﻿namespace DemoProject.Models
{
    public class FoulType
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual FoulDetail FoulDetail { get; set; }
    }
}
