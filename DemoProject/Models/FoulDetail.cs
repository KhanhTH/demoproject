﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoProject.Models
{
    public class FoulDetail
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FoulID { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FoulTypeID { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime TimeOccurred { get; set; }
        public string Location { get; set; }
        public virtual Foul Foul { get; set; }
        public virtual FoulType FoulType { get; set; }
    }
}
