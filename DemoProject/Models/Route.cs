﻿namespace DemoProject.Models
{
    public class Route
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual Foul Foul { get; set; }
    }
}
