﻿namespace DemoProject.Models
{
    public class Foul
    {
        public int ID { get; set; }
        public int DriverID { get; set; }
        public int RouteID { get; set; }

        public virtual Driver Driver { get; set; }
        public virtual Route Route { get; set; }
        public virtual FoulDetail FoulDetail { get; set; }
    }
}
