﻿using DemoProject.Models;
using System;
using System.Linq;

namespace DemoProject.Data
{
    public static class DBInitializer
    {
        public static void Initialize(DBContext context)
        {
            context.Database.EnsureCreated();

            if (context.Companies.Any())
            {
                return;
            }

            // Initialize Company
            var companies = new Company[]
            {
                new Company{Name="ABC"},
                new Company{Name="DEF"}
            };
            foreach (Company c in companies)
            {
                context.Companies.Add(c);
            }
            context.SaveChanges();

            // Initialize Driver
            var drivers = new Driver[]
            {
                new Driver{Name="Alex",CompanyID=1},
                new Driver{Name="Bowser",CompanyID=1},
                new Driver{Name="Dave",CompanyID=2},
            };
            foreach (Driver d in drivers)
            {
                context.Drivers.Add(d);
            }
            context.SaveChanges();

            // Initialize Route
            var routes = new Route[]
            {
                new Route{Name="SGN-HAN"},
                new Route{Name="SGN-DAN"},
                new Route{Name="DAN-HAN"},
            };
            foreach (Route r in routes)
            {
                context.Routes.Add(r);
            }
            context.SaveChanges();

            // Initialize FoulType
            var foulTypes = new FoulType[]
            {
                new FoulType{Name="Camera"},
                new FoulType{Name="GPS"}
            };
            foreach (FoulType ft in foulTypes)
            {
                context.FoulTypes.Add(ft);
            }
            context.SaveChanges();

            // Initialize Foul
            var fouls = new Foul[]
            {
                new Foul{DriverID=1,RouteID=1},
                new Foul{DriverID=1,RouteID=2},
                new Foul{DriverID=1,RouteID=3},
                new Foul{DriverID=2,RouteID=2},
                new Foul{DriverID=3,RouteID=3}
            };
            foreach (Foul f in fouls)
            {
                context.Fouls.Add(f);
            }
            context.SaveChanges();

            // Initialize FoulDetail
            var foulDetails = new FoulDetail[]
            {
                new FoulDetail{FoulID=1,FoulTypeID=1,TimeOccurred=DateTime.Parse("2019-08-20"),Location="DAL"},
                new FoulDetail{FoulID=2,FoulTypeID=1,TimeOccurred=DateTime.Parse("2019-08-19"),Location="NHA"},
                new FoulDetail{FoulID=3,FoulTypeID=2,TimeOccurred=DateTime.Parse("2019-08-18"),Location="PTH"},
                new FoulDetail{FoulID=4,FoulTypeID=1,TimeOccurred=DateTime.Parse("2019-08-21"),Location="PRN"},
                new FoulDetail{FoulID=5,FoulTypeID=2,TimeOccurred=DateTime.Parse("2019-08-22"),Location="HOA"}
            };
            foreach (FoulDetail fd in foulDetails)
            {
                context.FoulDetails.Add(fd);
            }
            context.SaveChanges();
        }
    }
}
