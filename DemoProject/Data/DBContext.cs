﻿using DemoProject.Models;
using Microsoft.EntityFrameworkCore;

namespace DemoProject.Data
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Foul> Fouls { get; set; }
        public DbSet<FoulDetail> FoulDetails { get; set; }
        public DbSet<FoulType> FoulTypes { get; set; }
        public DbSet<Route> Routes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>().ToTable("Company");
            modelBuilder.Entity<Driver>().ToTable("Driver");
            modelBuilder.Entity<Foul>().ToTable("Foul");
            modelBuilder.Entity<FoulDetail>().ToTable("FoulDetail");
            modelBuilder.Entity<FoulType>().ToTable("FoulType");
            modelBuilder.Entity<Route>().ToTable("Route");

            modelBuilder.Entity<FoulDetail>().HasKey(c => new { c.FoulID, c.FoulTypeID });
        }
    }
}
