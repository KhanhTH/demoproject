﻿using DemoProject.Data;
using DemoProject.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DemoProject.Services
{
    public class CompanyServices : ICompanyServices
    {
        public async Task<List<CompanyFoulDetails>> GetCompanyFoulDetailsService(DBContext _context, string companyName, string fromDate, string toDate, string foulType)
        {
            var company = new Company();
            var result = new List<CompanyFoulDetails>();

            company = await _context.Companies.Include(x => x.Drivers).FirstOrDefaultAsync(c => c.Name == companyName);
            List<int> driverIds = company.Drivers.Select(d => d.ID).ToList();

            if (company != null && driverIds.Count > 0)
            {
                foreach (int driverId in driverIds)
                {
                    var item = new CompanyFoulDetails();
                    item.Name = company.Name;
                    var foul = _context.Fouls.Include(x => x.FoulDetail).ThenInclude(x => x.FoulType).Where(x => x.DriverID == driverId 
                            && DateTime.Compare(x.FoulDetail.TimeOccurred, DateTime.ParseExact(fromDate, "ddMMyyyy", CultureInfo.InvariantCulture)) >= 0
                            && DateTime.Compare(x.FoulDetail.TimeOccurred, DateTime.ParseExact(toDate, "ddMMyyyy", CultureInfo.InvariantCulture)) <= 0
                            && x.FoulDetail.FoulType.Name.Equals(foulType));

                    foreach (Foul f in foul)
                    {
                        item.FoulDate = f.FoulDetail.TimeOccurred;
                        item.FoulType = f.FoulDetail.FoulType.Name;
                        result.Add(item);
                    }
                }
            }

            return result;
        }
    }
}
