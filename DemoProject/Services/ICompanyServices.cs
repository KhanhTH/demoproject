﻿using DemoProject.Data;
using DemoProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoProject.Services
{
    public interface ICompanyServices
    {
        Task<List<CompanyFoulDetails>> GetCompanyFoulDetailsService(DBContext _context, string companyName, string fromDate, string toDate, string foulType);
    }
}
